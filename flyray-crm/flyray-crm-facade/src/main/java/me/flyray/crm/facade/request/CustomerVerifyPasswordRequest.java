package me.flyray.crm.facade.request;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import me.flyray.crm.facade.BaseRequest;

import javax.validation.constraints.NotNull;

/**
 * 校验交易密码请求
 * @author centerroot
 * @time 创建时间:2018年9月25日上午10:42:46
 * @description
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
@ApiModel(value = "校验交易密码请求")
public class CustomerVerifyPasswordRequest extends BaseRequest {

	@NotNull(message = "平台编号不能为空")
	@ApiModelProperty("平台编号")
	private String platformId;
	
//	@NotNull(message = "用户编号不能为空")
	@ApiModelProperty(value = "用户编号")
	private String customerId;
	
	@ApiModelProperty(value = "身份证号")
	private String credNo;
	
	@ApiModelProperty(value = "姓名")
	private String realName;
	
	@NotNull(message="交易密码不能为空")
	@ApiModelProperty(value = "交易密码")
	private String password;

}
