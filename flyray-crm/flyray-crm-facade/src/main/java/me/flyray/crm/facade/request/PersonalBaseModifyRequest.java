package me.flyray.crm.facade.request;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotNull;
import java.io.Serializable;


/**
 * 个人客户基础信息请求实体
 * @author centerroot
 * @time 创建时间:2018年7月16日下午4:24:07
 * @description
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
@ApiModel(value = "个人客户基础信息请求参数")
public class PersonalBaseModifyRequest implements Serializable {


	@ApiModelProperty(value = "平台编号")
    private String platformId;

	@NotNull(message="参数[personalId]不能为空")
	@ApiModelProperty(value = "用户编码")
	private String personalId;

	@ApiModelProperty(value = "用户名称")
    private String realName;
	
    @ApiModelProperty(value = "性别：1—男，2—女")
    private String sex;
	
	@ApiModelProperty(value = "生日")
    private String birthday;
	
	//手机号
	@ApiModelProperty(value = "手机号")
	private String phone;
	
	//备注
	@ApiModelProperty(value = "备注")
	private String remark;
    
	    //客户等级
	@ApiModelProperty(value = "客户等级")
    private String personalLevel;

	    //电子邮件
	@ApiModelProperty(value = "电子邮件")
    private String email;

}
