package me.flyray.crm.facade.request;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotNull;
import java.io.Serializable;


/**
 * 平台功能请求实体
 * @author chj
 * @time 创建时间:2018年7月16日下午4:24:07
 * @description
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
@ApiModel(value = "平台功能请求请求参数")
public class PlatformFunctionConfigRequestParam implements Serializable {
	
	@ApiModelProperty(value = "序号")
    private String id;
	
	@NotNull(message="代码不能为空")
	@ApiModelProperty(value = "代码")
    private String code;
	
	@NotNull(message="代码不能为空")
	@ApiModelProperty(value = "级别")
    private String level;
	
	@NotNull(message="代码不能为空")
	@ApiModelProperty(value = "渠道")
    private String channel;
	
	@NotNull(message="代码不能为空")
	@ApiModelProperty(value = "是否展示")
    private String isshow;
	
	
	@ApiModelProperty(value = "父级菜单")
    private String ppCode;
	
	@NotNull(message="平台编号不能为空")
	@ApiModelProperty(value = "平台编号")
    private String platformId;
	
}
