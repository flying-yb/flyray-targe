package me.flyray.crm.facade.request;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotNull;

@Data
@NoArgsConstructor
@AllArgsConstructor
@ApiModel("个人或者商户入账接口参数")
public class IntoAccountRequest {
	@NotNull(message = "平台编号不能为空")
	@ApiModelProperty("平台编号")
	private String platformId;
	
	@NotNull(message = "订单号不能为空")
	@ApiModelProperty("订单号")
	private String orderNo;
	
	@NotNull(message = "交易金额不能为空")
	@ApiModelProperty("交易金额")
	private String intoAccAmt;

	@NotNull(message = "客户编号不能为空")
	@ApiModelProperty("客户编号")
	private String customerId;

	@ApiModelProperty("商户编号")
	private String merchantId;

	@ApiModelProperty("商户名称")
	private String merchantName;

	@ApiModelProperty("商户类型编号")
	private String merchantType;	
	
	@NotNull(message = "客户类型不能为空")
	@ApiModelProperty("客户类型")
	private String customerType;

    @NotNull(message = "账户名称不能为空")
    @ApiModelProperty("账户名称")
    private String accountName;
	
	@NotNull(message = "账户类型不能为空")
	@ApiModelProperty("账户类型")
	private String accountType;

	@ApiModelProperty("个人客户编号")
	private String personalId;

	@NotNull(message = "交易类型不能为空")
	@ApiModelProperty("交易类型")
	private String tradeType;
	
	@ApiModelProperty("手续费")
	private String txFee;

	@ApiModelProperty("来向资金账号")
	private String fromAccount;

	@ApiModelProperty("来向资金账号名称")
	private String fromAccountName;
}
