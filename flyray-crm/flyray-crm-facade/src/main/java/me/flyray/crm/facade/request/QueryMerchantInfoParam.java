package me.flyray.crm.facade.request;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotNull;
import java.io.Serializable;

/**
 * 微信小程序商户信息查询
 * @author c
 *
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
@ApiModel(value = "微信小程序商户信息查询请求参数")
public class QueryMerchantInfoParam implements Serializable {
	
	@NotNull(message="平台编号不能为空")
	@ApiModelProperty(value = "平台编号")
	private String platformId;
	
	@NotNull(message="商户编号不能为空")
	@ApiModelProperty(value = "商户编号")
    private String merchantId;
	
}
