package me.flyray.crm.facade.feignclient.modules.wechat;

import me.chanjar.weixin.mp.bean.result.WxMpUser;
import me.flyray.common.msg.BaseApiResponse;
import me.flyray.crm.facade.request.MerchantBalanceOpRequest;
import me.flyray.crm.facade.response.MerchantAccountOpBalanceResp;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

/**
 * @Author: bolei
 * @date: 16:02 2019/2/11
 * @Description: 微信授权服务
 */

@FeignClient(value = "flyray-crm-core")
public interface WxAuthServiceClient {

    /**
     *  微信授权服务 通过code换取openid或其他用户信息
     *  scope参数：snsapi_userinfo or snsapi_base
     * @param appId
     * @return
     */
    @RequestMapping(value = "feign/wx/auth",method = RequestMethod.POST)
    BaseApiResponse<WxMpUser> wxAuth(@RequestParam("appId") String appId, @RequestParam("code") String code, @RequestParam("scope") String scope);

}
