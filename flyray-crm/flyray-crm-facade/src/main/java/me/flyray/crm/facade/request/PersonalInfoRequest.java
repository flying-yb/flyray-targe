package me.flyray.crm.facade.request;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.util.Date;

@Data
@NoArgsConstructor
@AllArgsConstructor
@ApiModel(value = "查询平台支持账户请求参数")
public class PersonalInfoRequest implements Serializable {
	@ApiModelProperty(value = "当前页码")
	private int page;

	@ApiModelProperty(value = "每页条数")
	private int limit;

	// 序号
	@ApiModelProperty(value = "序号")
	private Integer id;

	// 平台编号
	@NotNull(message = "平台编号不能为空")
	@ApiModelProperty(value = "平台编号")
	private String platformId;
	
	    //个人客户编号
	@ApiModelProperty(value = "个人客户编号")
    private String personalId;
	
	    //用户编号
	@ApiModelProperty(value = "用户编号")
    private String customerId;
	
	    //第三方会员编号
	@ApiModelProperty(value = "第三方会员编号")
    private String thirdNo;
	
	    //手机号
	@ApiModelProperty(value = "手机号")
	private String phone;
	
	    //用户名称
	@ApiModelProperty(value = "用户名称")
    private String realName;
	
	    //身份证号
	@ApiModelProperty(value = "身份证号")
    private String idCard;
	
	    //用户昵称
	@ApiModelProperty(value = "用户昵称")
    private String nickname;
	
	    //性别 1：男 2：女
	@ApiModelProperty(value = "性别")
    private String sex;
	
	    //生日
	@ApiModelProperty(value = "生日")
    private String birthday;
	
	    //居住地
	@ApiModelProperty(value = "居住地")
    private String address;
	
	    //身份证正面
	@ApiModelProperty(value = "身份证正面")
    private String idPositive;
	
	    //身份证反面
	@ApiModelProperty(value = "身份证反面")
    private String idNegative;
	
	    //认证状态  00：未认证  01：无需认证  02：认证成功  03：认证失败
	@ApiModelProperty(value = "认证状态")
    private String authenticationStatus;
	
	    //账户状态 00：正常，01：冻结
	@ApiModelProperty(value = "账户状态")
    private String status;
	
	    //创建时间
	@ApiModelProperty(value = "创建时间")
    private Date createTime;
	
	    //更新时间
	@ApiModelProperty(value = "更新时间")
    private Date updateTime;
	
    // 归属人（存后台管理系统登录人员id）指谁发展的客户
	@ApiModelProperty(value = "归属人")
    private Long owner;

    // 用户头像
	@ApiModelProperty(value = "用户头像")
    private String avatar;
    
	    //备注
	@ApiModelProperty(value = "备注")
	private String remark;
	
	    //个人客户类型  00：注册用户  01：运营添加用户
	@ApiModelProperty(value = "个人客户类型")
	private String personalType;
	
	    //客户等级
	@ApiModelProperty(value = "客户等级")
    private Integer personalLevel;
	
	    //行业
	@ApiModelProperty(value = "行业")
    private String industry;
	
	    //行业描述
	@ApiModelProperty(value = "行业描述")
    private String industryDescription;
	
	    //职务
	@ApiModelProperty(value = "职务")
    private String job;
	
	    //所在省市
	@ApiModelProperty(value = "所在省市")
    private String provinces;
	
	    //电子邮件
	@ApiModelProperty(value = "电子邮件")
    private String email;
	
	    //邮编
	@ApiModelProperty(value = "邮编")
    private String zipCode;
    
	    //关注行业
	@ApiModelProperty(value = "关注行业")
	private String attentionIndustry;
	
	    //关注职位
	@ApiModelProperty(value = "关注职位")
	private String attentionJobs;
	
		//标签
	@ApiModelProperty(value = "标签")
	private String selfTag;
	
		//设备ID
	@ApiModelProperty(value = "设备ID")
	private String deviceId;
	
		//设备类型
	@ApiModelProperty(value = "设备类型")
	private String deviceType;
	
		//登录密码
	@ApiModelProperty(value = "登录密码")
	private String credential;	
	
		//消息推送tag
	@ApiModelProperty(value = "消息推送tag")
	private String pushTag;
	
		//消息推送alias
	@ApiModelProperty(value = "消息推送alias")
	private String pushAlias;
	
		//消息推送RegistrationID
	@ApiModelProperty(value = "消息推送RegistrationID")
	private String pushRegistrationId;
	
	//登录密码错误次数
	@ApiModelProperty(value = "登录密码错误次数")
	private Integer passwordErrorCount;
	
}
