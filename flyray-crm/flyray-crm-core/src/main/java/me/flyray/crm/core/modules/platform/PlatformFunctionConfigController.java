package me.flyray.crm.core.modules.platform;

import java.util.Map;

import javax.validation.Valid;

import me.flyray.common.msg.ResponseCode;
import me.flyray.crm.core.entity.PlatformFunctionConfig;
import me.flyray.crm.core.biz.platform.PlatformFunctionConfigBiz;
import me.flyray.crm.facade.request.PlatformFunctionConfigRequestParam;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import me.flyray.common.msg.TableResultResponse;
import me.flyray.common.rest.BaseController;
import me.flyray.common.util.ResponseHelper;

@Controller
@RequestMapping("platformFunctionConfig")
public class PlatformFunctionConfigController extends BaseController<PlatformFunctionConfigBiz, PlatformFunctionConfig> {

	private static final Logger logger = LoggerFactory.getLogger(PlatformFunctionConfigController.class);
	
	/**
	 * 查询平台功能配置
	 * @param param
	 * @return
	 */
    @ResponseBody
    @RequestMapping(value = "/list", method = RequestMethod.GET)
	public TableResultResponse<PlatformFunctionConfig> query(@RequestParam Map<String, Object> param) {
    	logger.info("查询平台功能配置。。。{}"+param);
		return baseBiz.queryList(param);
	}
    
    /**
     * 添加
     * @param param
     * @return
     */
    @ResponseBody
    @RequestMapping(value = "/add", method = RequestMethod.POST)
	public Map<String, Object> add(@Valid @RequestBody PlatformFunctionConfigRequestParam param) {
    	logger.info("添加平台功能配置。。。{}"+param);
    	PlatformFunctionConfig func = baseBiz.addFunction(param);
    	if (func == null) {
    		return ResponseHelper.success(null, null, ResponseCode.OK.getCode(), ResponseCode.OK.getMessage());
		}else {
			return ResponseHelper.success(null, null, ResponseCode.SYSTEM_ERROR.getCode(), ResponseCode.SYSTEM_ERROR.getMessage());
		}
	}
    
    
    /**
     * 删除
     * @param param
     * @return
     */
    @ResponseBody
    @RequestMapping(value = "/delete", method = RequestMethod.POST)
	public Map<String, Object> delete(@RequestBody PlatformFunctionConfigRequestParam param) {
    	logger.info("删除平台功能配置。。。{}"+param);
		baseBiz.deleteFunction(param);
		return ResponseHelper.success(null, null, "200", "删除成功");
	}
    
    @ResponseBody
    @RequestMapping(value = "/update", method = RequestMethod.POST)
	public Map<String, Object> update(@RequestBody PlatformFunctionConfigRequestParam param) {
    	logger.info("修改平台功能配置。。。{}"+param);
		baseBiz.updateFunction(param);
		return ResponseHelper.success(null, null, "200", "修改成功");
	}
}