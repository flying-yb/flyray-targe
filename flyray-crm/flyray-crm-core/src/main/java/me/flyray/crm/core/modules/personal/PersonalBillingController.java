package me.flyray.crm.core.modules.personal;

import me.flyray.crm.core.entity.PersonalBilling;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import me.flyray.common.rest.BaseController;
import me.flyray.crm.core.biz.personal.PersonalBillingBiz;

@RestController
@RequestMapping("personalBilling")
public class PersonalBillingController extends BaseController<PersonalBillingBiz, PersonalBilling> {

}