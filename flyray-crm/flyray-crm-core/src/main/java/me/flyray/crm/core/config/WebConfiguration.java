package me.flyray.crm.core.config;

import java.util.ArrayList;
import java.util.Collections;

import me.flyray.common.exception.GlobalExceptionHandler;
import me.flyray.common.interceptor.ValidateParamInterceptor;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Primary;
import org.springframework.web.servlet.config.annotation.InterceptorRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

import me.flyray.auth.client.interceptor.ServiceAuthRestInterceptor;
import me.flyray.auth.client.interceptor.UserAuthRestInterceptor;

/**
 *
 * @author ace
 * @date 2017/9/8
 */
@Configuration("crmCoreWebConfig")
@Primary
public class WebConfiguration implements WebMvcConfigurer {
    @Bean
    GlobalExceptionHandler getGlobalExceptionHandler() {
        return new GlobalExceptionHandler();
    }

    @Override
    public void addInterceptors(InterceptorRegistry registry) {
        ArrayList<String> commonExcludePathPatterns = getExcludeCommonPathPatterns();
        registry.addInterceptor(getServiceAuthRestInterceptor()).
                addPathPatterns("/**").excludePathPatterns(commonExcludePathPatterns.toArray(new String[]{}));
        commonExcludePathPatterns.add("/feign/**");
        registry.addInterceptor(getUserAuthRestInterceptor()).
                addPathPatterns("/**").excludePathPatterns(commonExcludePathPatterns.toArray(new String[]{}));
    }

    @Bean
    ServiceAuthRestInterceptor getServiceAuthRestInterceptor() {
        return new ServiceAuthRestInterceptor();
    }

    @Bean
    UserAuthRestInterceptor getUserAuthRestInterceptor() {
        return new UserAuthRestInterceptor();
    }
    
    @Bean
    ValidateParamInterceptor getValidateParamInterceptor() {
        return new ValidateParamInterceptor();
    }
    /**
     * 需要用户和服务认证判断的路径
     * @return
     */
    private ArrayList<String> getIncludePathPatterns() {
        ArrayList<String> list = new ArrayList<>();
        String[] urls = {
                "/**"
        };
        Collections.addAll(list, urls);
        return list;
    }
    
    private ArrayList<String> getExcludeCommonPathPatterns() {
        ArrayList<String> list = new ArrayList<>();
        String[] urls = {
                "/v2/api-docs",
                "/api/user/validate",
                "/swagger-resources/**",
                "/cache/**",
                "/api/log/save",
                "/activity/*",
                "/coupon/*",
                "/incident/*",
                "/integral/*",
                "/common/*",
                "/marScene/*",
                "/job/*",
                "/scene/*",
                "/jobLog/*",
                "/customer/queryCustomerinfoByMobile"
        };
        Collections.addAll(list, urls);
        return list;
    }

	/*@Override
	public void addResourceHandlers(ResourceHandlerRegistry registry) {
		WebMvcConfigurer.super.addResourceHandlers(registry);
		registry.addResourceHandler("/uploadFile/**").addResourceLocations("file:D:/pj_bounty/bounty-hunter/flyray-crm-core/pics/");
	}*/

}
