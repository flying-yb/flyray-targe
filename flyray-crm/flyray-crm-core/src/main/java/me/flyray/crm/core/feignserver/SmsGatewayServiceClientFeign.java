package me.flyray.crm.core.feignserver;

import me.flyray.common.msg.BaseApiResponse;
import me.flyray.crm.core.biz.SMSBiz;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import me.flyray.crm.facade.feignclient.modules.sms.SmsGatewayServiceClient;
import me.flyray.crm.facade.request.SendSMSRequest;
import me.flyray.crm.facade.request.ValidateSMSRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.validation.Valid;
import java.util.Map;

/***
 * 短信相关服务
 * */
@Api(tags="短信相关服务")
@Controller
@RequestMapping("feign/sms")
public class SmsGatewayServiceClientFeign  {

	@Autowired
	private SMSBiz smsBiz;

	/**
	 * 发送短信并保存到Redis中
	 * @author centerroot
	 * @time 创建时间:2018年11月19日下午5:12:31
	 * @param sendSMSRequest
	 * @return
	 */
	@ApiOperation("发送短信并保存到Redis中")
	@ResponseBody
	@RequestMapping(value = "/sendCode",method = RequestMethod.POST)
	public BaseApiResponse sendCode(@RequestBody @Valid SendSMSRequest sendSMSRequest){
		smsBiz.sendSmsCode(sendSMSRequest);
		return BaseApiResponse.newSuccess();
	}


	@ApiOperation("校验短信验证码")
	@ResponseBody
	@RequestMapping(value = "/validateSmsCode",method = RequestMethod.POST)
	public BaseApiResponse validateSmsCode(@RequestBody @Valid ValidateSMSRequest validateSMSRequest){
		smsBiz.validateSmsCode(validateSMSRequest);
		return BaseApiResponse.newSuccess();
	}

}
