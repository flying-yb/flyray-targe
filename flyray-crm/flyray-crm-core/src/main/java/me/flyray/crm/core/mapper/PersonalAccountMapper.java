package me.flyray.crm.core.mapper;

import java.util.List;

import me.flyray.common.vo.QueryPersonalAccountRequest;
import me.flyray.common.vo.QueryPersonalAccountResponse;
import me.flyray.crm.core.entity.PersonalAccount;

import me.flyray.crm.core.entity.PersonalPointAccount;
import org.apache.ibatis.annotations.Param;
import tk.mybatis.mapper.common.Mapper;

/**
 * 个人账户信息
 * 
 * @author centerroot
 * @email ${email}
 * @date 2018-07-16 10:15:49
 */
@org.apache.ibatis.annotations.Mapper
public interface PersonalAccountMapper extends Mapper<PersonalAccount> {
	
	/**
	 * 查询原力值前三的账户信息
	 */
	public List<PersonalAccount> selectTopList(PersonalAccount personalAccount);

	PersonalPointAccount selectPointAccount(@Param("request") QueryPersonalAccountRequest request);
}
