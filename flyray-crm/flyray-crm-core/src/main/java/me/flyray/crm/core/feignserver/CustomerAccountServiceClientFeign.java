package me.flyray.crm.core.feignserver;

import me.flyray.common.msg.BaseApiResponse;
import me.flyray.crm.core.biz.customer.CustomerAccountQueryBiz;
import me.flyray.crm.core.biz.customer.CustomerAccountTransferBiz;
import me.flyray.crm.core.biz.customer.CustomerIntoAccountBiz;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import me.flyray.crm.core.biz.customer.CustomerOutAccountBiz;
import me.flyray.crm.facade.feignclient.modules.customer.CustomerAccountServiceClient;
import me.flyray.crm.facade.feignclient.modules.personal.PersonalBaseServiceClient;
import me.flyray.crm.facade.request.*;
import me.flyray.crm.facade.response.AccountJournalQueryResponse;
import me.flyray.crm.facade.response.CustomerAccountQueryResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.validation.Valid;
import java.util.List;
import java.util.Map;

/***
 * 入账相关的接口
 * */
@Api(tags="入账相关的接口")
@Controller
public class CustomerAccountServiceClientFeign implements CustomerAccountServiceClient {
	
	@Autowired
	private CustomerIntoAccountBiz customerIntoAccountBiz;
	@Autowired
	private CustomerOutAccountBiz outAccountBiz;
	@Autowired
	private CustomerAccountTransferBiz customerAccountTransferBiz;
	@Autowired
	private CustomerAccountQueryBiz commonAccountQueryBiz;
	
	/**
	 * @param
	 * @des  根据交易类型增加总交易账户
	 * @return
	 */
	@Override
	@ResponseBody
    public BaseApiResponse<Void> intoAccount(@RequestBody @Valid IntoAccountRequest intoAccountRequest){
		customerIntoAccountBiz.intoAccount(intoAccountRequest);
		return BaseApiResponse.newSuccess();
    }

	/**
	 * 个人或者商户直接出账的接口
	 * */
	@Override
	@ResponseBody
	public BaseApiResponse<Void> outAccount(@RequestBody @Valid OutAccountRequest outAccountRequest){
		outAccountBiz.outAccount(outAccountRequest);
		return BaseApiResponse.newSuccess();
	}

	/***
	 * 个人或者商户解冻并出账的接口
	 * */
	@Override
	@ResponseBody
	public BaseApiResponse<Void> unfreezeAndOutAccount(@RequestBody @Valid UnfreezeAndOutAccountRequest unFreezeAndOutAccountRequest){
		outAccountBiz.unfreezeAndOutAccount(unFreezeAndOutAccountRequest);
		return BaseApiResponse.newSuccess();
	}

	/**
	 * @param
	 * @return
	 */
	@Override
	@ResponseBody
	public Map<String, Object> accountTransfer(@RequestBody @Valid AccountTransferRequest accountTransferRequest){
		Map<String, Object> response = customerAccountTransferBiz.accountTransfer(accountTransferRequest);
		return response;
	}

	/***
	 * 个人或者商户解冻并转账接口
	 * 适用于提现
	 * */
	@Override
	@ResponseBody
	public BaseApiResponse<Void> unfreezeAndTransfer(@RequestBody @Valid UnfreezeAndTransferRequest unFreezeAndTransferRequest){
		customerAccountTransferBiz.unfreezeAndTransfer(unFreezeAndTransferRequest);
		return BaseApiResponse.newSuccess();
	}

	/**
	 * 账户查询
	 * @param
	 * @return withdrawApply
	 */
	@Override
	@ResponseBody
	public BaseApiResponse<List<CustomerAccountQueryResponse>> accountQuery(@RequestBody @Valid AccountQueryRequest accountQueryRequest){
		List<CustomerAccountQueryResponse> response = commonAccountQueryBiz.accountQuery(accountQueryRequest);
		return BaseApiResponse.newSuccess(response);
	}

	/**
	 * 查询账户交易流水
	 * @param accountJournalQueryRequest
	 * @return
	 */
	@Override
	@ResponseBody
	public BaseApiResponse<List<AccountJournalQueryResponse>> accountJournalQuery(@RequestBody @Valid AccountJournalQueryRequest accountJournalQueryRequest){
		List<AccountJournalQueryResponse> response = commonAccountQueryBiz.accountJournalQuery(accountJournalQueryRequest);
		return BaseApiResponse.newSuccess(response);
	}

	/**
	 * 查询账户详细交易流水
	 * 包含了账户对应的客户信息
	 * @param accountJournalQueryRequest
	 * @return
	 */
	@Override
	@ResponseBody
	public BaseApiResponse<List<AccountJournalQueryResponse>> accountDetailJournalQuery(@RequestBody @Valid AccountJournalQueryRequest accountJournalQueryRequest){
		List<AccountJournalQueryResponse> response = commonAccountQueryBiz.accountDetailJournalQuery(accountJournalQueryRequest);
		return BaseApiResponse.newSuccess(response);
	}
	
}
