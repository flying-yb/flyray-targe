package me.flyray.crm.core.mapper;

import me.flyray.crm.core.entity.PersonalCertificationInfo;
import tk.mybatis.mapper.common.Mapper;

/**
 * 用户实名认证信息表
 * 
 * @author centerroot
 * @email ${email}
 * @date 2018-09-10 15:02:35
 */
public interface PersonalCertificationInfoMapper extends Mapper<PersonalCertificationInfo> {
	
}
