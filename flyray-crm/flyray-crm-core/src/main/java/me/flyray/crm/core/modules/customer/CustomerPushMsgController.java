package me.flyray.crm.core.modules.customer;

import me.flyray.common.rest.BaseController;
import me.flyray.crm.core.biz.customer.CustomerPushMsgBiz;
import me.flyray.crm.core.entity.CustomerPushMsg;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
@RequestMapping("customerPushMsg")
public class CustomerPushMsgController extends BaseController<CustomerPushMsgBiz, CustomerPushMsg> {

}