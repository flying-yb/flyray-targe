package me.flyray.crm.core.modules.personal;

import me.flyray.crm.core.entity.PersonalDistributionRelation;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import me.flyray.common.rest.BaseController;
import me.flyray.crm.core.biz.personal.PersonalDistributionRelationBiz;

/**
 * 分销关系表设计 https://blog.csdn.net/cctcc/article/details/53992215
 */

@RestController
@RequestMapping("personalDistributionRelation")
public class PersonalDistributionRelationController extends BaseController<PersonalDistributionRelationBiz, PersonalDistributionRelation> {

}