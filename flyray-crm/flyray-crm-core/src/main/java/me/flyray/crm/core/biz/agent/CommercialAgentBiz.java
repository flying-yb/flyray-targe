package me.flyray.crm.core.biz.agent;

import lombok.extern.slf4j.Slf4j;
import me.flyray.common.constant.UserConstant;
import me.flyray.common.context.BaseContextHandler;
import me.flyray.common.enums.UserType;
import me.flyray.common.exception.BusinessException;
import me.flyray.common.msg.BaseApiResponse;
import me.flyray.common.msg.ResponseCode;
import me.flyray.common.util.EntityUtils;
import me.flyray.common.util.SnowFlake;
import me.flyray.common.vo.admin.AdminUserRequest;
import me.flyray.common.vo.admin.AdminUserResponse;
import me.flyray.crm.core.entity.PersonalBase;
import me.flyray.crm.core.entity.PersonalBaseExt;
import me.flyray.crm.core.mapper.PersonalBaseExtMapper;
import me.flyray.crm.core.mapper.PersonalBaseMapper;
import me.flyray.crm.facade.enumeration.IsAdmin;
import me.flyray.crm.facade.feignclient.modules.admin.AdminUserServiceClient;
import me.flyray.crm.facade.request.CommercialAgentRequest;
import me.flyray.crm.facade.request.PersonalBaseRequest;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.DigestUtils;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @Author: bolei
 * @date: 10:47 2019/2/14
 * @Description: 代理商处理逻辑
 */

@Slf4j
@Transactional(rollbackFor = Exception.class)
@Service
public class CommercialAgentBiz {

    @Autowired
    private AdminUserServiceClient adminUserServiceClient;
    @Autowired
    private PersonalBaseMapper personalBaseMapper;
    @Autowired
    private PersonalBaseExtMapper personalBaseExtMapper;

    /**
     * 添加代理商
     * @date: 10:47 2019/2/14
     * @param commercialAgentRequest
     * @return
     */
    public BaseApiResponse add(CommercialAgentRequest commercialAgentRequest){
        return BaseApiResponse.newSuccess();

    }

    /**
     * 删除代理商
     * @date: 10:47 2019/2/14
     * @param userId
     * @return
     */
    public BaseApiResponse delete(String userId){

        return BaseApiResponse.newSuccess();
    }

    /**
     * 查询代理商
     * @date: 10:47 2019/2/14
     * @param commercialAgentRequest
     * @return
     */
    public BaseApiResponse<List<AdminUserResponse>> list(CommercialAgentRequest commercialAgentRequest){

        AdminUserRequest param = new AdminUserRequest();
        if(!"1".equals(BaseContextHandler.getUserType())){
            param.setUserId(BaseContextHandler.getXId());
        }
        // 5:表示客户经理
        param.setUserType(UserType.AGENT_OPERATOR.getCode());
        param.setPlatformId(BaseContextHandler.getPlatformId());
        BaseApiResponse<List<AdminUserResponse>> result = adminUserServiceClient.getUserList(param);

        return result;
    }


    /**
     * 更新代理商
     * @date: 10:47 2019/2/14
     * @param commercialAgentRequest
     * @return
     */
    public BaseApiResponse update(CommercialAgentRequest commercialAgentRequest){
        return BaseApiResponse.newSuccess();
    }

}
