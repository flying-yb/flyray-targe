package me.flyray.crm.core.modules.wechat;

import me.chanjar.weixin.common.error.WxErrorException;
import me.chanjar.weixin.mp.api.WxMpService;
import me.chanjar.weixin.mp.bean.result.WxMpOAuth2AccessToken;
import me.chanjar.weixin.mp.bean.result.WxMpUser;
import me.flyray.crm.core.config.WxMpConfiguration;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

/**
 * @Author: bolei
 * @date: 13:48 2019/2/11
 * @Description: 微信重定向获取用户信息
 */

@Controller
@RequestMapping("/wx/redirect/")
public class WxRedirectController {

    @RequestMapping("/greet")
    public String greetUser(@RequestParam String appid, @RequestParam String code) {

        WxMpService mpService = WxMpConfiguration.getMpServices().get(appid);

        try {
            WxMpOAuth2AccessToken accessToken = mpService.oauth2getAccessToken(code);
            WxMpUser user = mpService.oauth2getUserInfo(accessToken, null);
            //重定向到页面
        } catch (WxErrorException e) {
            e.printStackTrace();
        }

        return "greet_user";
    }
}
