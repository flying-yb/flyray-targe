package me.flyray.crm.core.mapper;

import me.flyray.crm.core.entity.PlatformCoinAccout;

import tk.mybatis.mapper.common.Mapper;

/**
 * 
 * 
 * @author centerroot
 * @email ${email}
 * @date 2018-12-21 15:00:16
 */
@org.apache.ibatis.annotations.Mapper
public interface PlatformCoinAccoutMapper extends Mapper<PlatformCoinAccout> {
	
}
