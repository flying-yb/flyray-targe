package me.flyray.crm.core.api;

import java.util.Map;

import javax.validation.Valid;

import me.flyray.common.msg.BaseApiResponse;
import me.flyray.crm.core.biz.SMSBiz;
import me.flyray.crm.facade.request.SendSMSRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

@Api(tags="短信服务管理")
@Controller
@RequestMapping("smsManage")
public class SMSController {
	
	@Autowired
	private SMSBiz smsBiz;
	
	/**
	 * 发送短信验证码
	 * @author centerroot
	 * @time 创建时间:2018年9月8日下午4:01:12
	 * @param sendSMSRequest
	 * @return
	 */
	@ApiOperation("发送短信验证码")
	@RequestMapping(value = "/sendSmsCode",method = RequestMethod.POST)
    @ResponseBody
    public BaseApiResponse sendSmsCode(@RequestBody @Valid SendSMSRequest sendSMSRequest) {
		smsBiz.sendSmsCode(sendSMSRequest);
		return BaseApiResponse.newSuccess();
    }

}
