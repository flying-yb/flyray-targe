package me.flyray.crm.core.biz.customer;

import java.math.BigDecimal;
import java.sql.Timestamp;
import java.util.*;

import javax.validation.Valid;

import me.flyray.common.biz.BaseBiz;
import me.flyray.common.constant.UserConstant;
import me.flyray.common.enums.AccountState;
import me.flyray.common.enums.CustomerType;
import me.flyray.common.exception.BusinessException;
import me.flyray.common.msg.BizResponseCode;
import me.flyray.common.msg.ResponseCode;
import me.flyray.common.msg.TableResultResponse;
import me.flyray.common.util.*;
import me.flyray.crm.core.biz.SMSBiz;
import me.flyray.crm.core.entity.*;
import me.flyray.crm.core.mapper.*;
import me.flyray.crm.core.util.EncryptComp;
import me.flyray.crm.facade.request.*;
import org.apache.commons.lang.ObjectUtils;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.DigestUtils;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;

import com.github.pagehelper.Page;
import com.github.pagehelper.PageHelper;


import io.swagger.annotations.ApiParam;
import lombok.extern.slf4j.Slf4j;
import tk.mybatis.mapper.entity.Example;
import tk.mybatis.mapper.entity.Example.Criteria;

/**
 * 客户基础信息
 *
 * @author centerroot
 * @email ${email}
 * @date 2018-07-16 10:15:49
 */
@Slf4j
@Transactional(rollbackFor = Exception.class)
@Service
public class CustomerBaseBiz extends BaseBiz<CustomerBaseMapper, CustomerBase> {

    @Value("${crm.account.balanceSaltValue}")
    private String balanceSaltValue;
	@Autowired
	public CustomerBaseAuthMapper customerBaseAuthsMapper;
	@Autowired
	public PersonalBaseMapper personalBaseMapper;
	@Autowired
	public PersonalBaseExtMapper personalBaseExtMapper;
	@Autowired
	public MerchantBaseMapper merchantBaseMapper;
	@Autowired
	private MerchantAccountMapper merchantAccountMapper;
	@Autowired
	public SMSBiz smsBiz;
	@Autowired
	private PersonalCertificationInfoMapper personalCertificationInfoMapper;
	@Autowired
	private PlatformSafetyConfigMapper platformSafetyConfigMapper;
	@Autowired
	private PlatformAccoutConfigMapper platformAccoutConfigMapper;
    @Autowired
    private MerchantAccountMapper merAccountMapper;

	private BCryptPasswordEncoder encoder = new BCryptPasswordEncoder(UserConstant.PW_ENCORDER_SALT);

	/**
	 * 查询客户基础信息列表
	 * 
	 * @author centerroot
	 * @time 创建时间:2018年7月16日下午6:02:38
	 * @param queryCustomerBaseListRequest
	 * @return
	 */
	public Map<String, Object> queryList(QueryCustomerBaseListRequest queryCustomerBaseListRequest) {
		log.info("【查询客户基础信息列表】   请求参数：{}", EntityUtils.beanToMap(queryCustomerBaseListRequest));
		Map<String, Object> respMap = new HashMap<String, Object>();
		String platformId = queryCustomerBaseListRequest.getPlatformId();
		String customerId = queryCustomerBaseListRequest.getCustomerId();
		String authenticationStatus = queryCustomerBaseListRequest.getAuthenticationStatus();
		String status = queryCustomerBaseListRequest.getStatus();
		int page = queryCustomerBaseListRequest.getPage();
		int limit = queryCustomerBaseListRequest.getLimit();
		Map<String, Object> params = new HashMap<String, Object>();
		params.put("page", page);
		params.put("limit", limit);
		Query query = new Query(params);
		Page<Object> result = PageHelper.startPage(query.getPage(), query.getLimit());

		Example example = new Example(PersonalBase.class);
		Criteria criteria = example.createCriteria();
		if (!StringUtils.isEmpty(platformId)) {
			criteria.andEqualTo("platformId", platformId);
		}
		if (!StringUtils.isEmpty(customerId)) {
			criteria.andEqualTo("customerId", customerId);
		}
		if (!StringUtils.isEmpty(authenticationStatus)) {
			criteria.andEqualTo("authenticationStatus", authenticationStatus);
		}
		if (!StringUtils.isEmpty(status)) {
			criteria.andEqualTo("status", status);
		}
		example.setOrderByClause("create_time desc");
		List<CustomerBase> list = mapper.selectByExample(example);

		TableResultResponse<CustomerBase> table = new TableResultResponse<CustomerBase>(result.getTotal(), list);
		respMap.put("body", table);
		respMap.put("code", BizResponseCode.OK.getCode());
		respMap.put("message", BizResponseCode.OK.getMessage());
		log.info("【查询客户基础信息列表】   响应参数：{}", respMap);
		return respMap;
	}

	/**
	 * 重置密码
	 * 
	 * @author centerroot
	 * @time 创建时间:2018年7月18日上午11:35:00
	 * @param resetPasswordRequest
	 * @return
	 */
	public Map<String, Object> resetPassword(ResetPasswordRequest resetPasswordRequest) {
		log.info("【重置密码】   请求参数：{}", EntityUtils.beanToMap(resetPasswordRequest));
		Map<String, Object> respMap = new HashMap<String, Object>();
		String customerId = resetPasswordRequest.getCustomerId();
		String phone = resetPasswordRequest.getPhone();
//		String password = "123456";
		int count = 0;
		if ("0".equals(resetPasswordRequest.getType())) {
			String passwordSalt = ObjectUtils.toString((new Random().nextInt() * (99999 - 10000 + 1)) + 10000);
			String passwordCiphertext = MD5.md5(resetPasswordRequest.getPassword() + passwordSalt);
			// 登录密码
			CustomerBaseAuth customerBaseAuths = new CustomerBaseAuth();
			customerBaseAuths.setCredential(passwordCiphertext);

			Example example = new Example(CustomerBaseAuth.class);
			Criteria criteria = example.createCriteria();
			criteria.andEqualTo("customerId", customerId);
			criteria.andEqualTo("authType", "00");// 00:登录授权
			criteria.andEqualTo("isthird", "00");// 00:站内登录
			count = customerBaseAuthsMapper.updateByExampleSelective(customerBaseAuths, example);
			
			CustomerBase customerBase = mapper.selectByPrimaryKey(customerId);
			customerBase.setPasswordSalt(passwordSalt);
			customerBase.setUpdateTime(new Timestamp(System.currentTimeMillis()));
			count = mapper.updateByPrimaryKey(customerBase);

		} else if ("1".equals(resetPasswordRequest.getType())) {
			// 支付密码
			CustomerBase customerBase = mapper.selectByPrimaryKey(customerId);
			String payPasswordSalt = EncryptComp.genRandomStr(8);
			String newPayPassword = EncryptComp.encrypt(resetPasswordRequest.getPassword(), payPasswordSalt);
			customerBase.setPayPasswordSalt(payPasswordSalt);
			customerBase.setPayPassword(newPayPassword);
			customerBase.setUpdateTime(new Timestamp(System.currentTimeMillis()));
			count = mapper.updateByPrimaryKey(customerBase);
		}

		if (count > 0) {
			respMap.put("code", BizResponseCode.OK.getCode());
			respMap.put("message", BizResponseCode.OK.getMessage());
		} else {
			respMap.put("code", BizResponseCode.PWD_RESET_ERROR.getCode());
			respMap.put("message", BizResponseCode.PWD_RESET_ERROR.getMessage());
		}

		log.info("【重置密码】   响应参数：{}", respMap);
		return respMap;
	}

	/**
	 * 修改客户状态
	 * 
	 * @author centerroot
	 * @time 创建时间:2018年7月18日上午11:35:12
	 * @param updateCustomerStatusRequest
	 * @return
	 */
	public Map<String, Object> updateStatus(UpdateCustomerStatusRequest updateCustomerStatusRequest) {
		log.info("【修改客户状态】   请求参数：{}", EntityUtils.beanToMap(updateCustomerStatusRequest));
		Map<String, Object> respMap = new HashMap<String, Object>();
		String status = updateCustomerStatusRequest.getStatus();
		CustomerBase customerBase = new CustomerBase();
		customerBase.setCustomerId(updateCustomerStatusRequest.getCustomerId());
		customerBase.setUpdateTime(new Timestamp(System.currentTimeMillis()));
		if ("2".equals(updateCustomerStatusRequest.getType())) {
			// 登录密码状态
			customerBase.setPasswordStatus(status);
			mapper.updateByPrimaryKeySelective(customerBase);
		} else if ("3".equals(updateCustomerStatusRequest.getType())) {
			// 支付密码状态
			customerBase.setPayPasswordStatus(status);
			mapper.updateByPrimaryKeySelective(customerBase);
		} else if ("4".equals(updateCustomerStatusRequest.getType())) {
			// 客户状态
			customerBase.setStatus(status);
			mapper.updateByPrimaryKeySelective(customerBase);
		}
		respMap.put("code", BizResponseCode.OK.getCode());
		respMap.put("message", BizResponseCode.OK.getMessage());
		log.info("【修改客户状态】   响应参数：{}", respMap);
		return respMap;
	}

	/**
	 * 查询单个客户基础信息详情
	 * 
	 * @author centerroot
	 * @time 创建时间:2018年7月17日上午11:41:36
	 * @param customerId
	 * @return
	 */
	public Map<String, Object> queryOneByKeys(String customerId) {
		log.info("【查询单个客户基础信息详情】   请求参数：{}", customerId);
		Map<String, Object> respMap = new HashMap<String, Object>();
		CustomerBase customerBaseReq = new CustomerBase();
		customerBaseReq.setCustomerId(customerId);
		CustomerBase customerBase = mapper.selectOne(customerBaseReq);
		respMap.put("CustomerBase", customerBase);
		respMap.put("code", BizResponseCode.OK.getCode());
		respMap.put("message", BizResponseCode.OK.getMessage());
		log.info("【查询单个客户基础信息详情】   响应参数：{}", respMap);
		return respMap;
	}

	/**
	 * 添加客户基础信息
	 * 
	 * @author centerroot
	 * @time 创建时间:2018年7月16日下午6:02:15
	 * @param customerBaseRequest
	 * @return
	 */
	public Map<String, Object> add(CustomerBaseRequest customerBaseRequest) {
		log.info("【添加客户基础信息】   请求参数：{}", EntityUtils.beanToMap(customerBaseRequest));
		Map<String, Object> respMap = new HashMap<String, Object>();
		CustomerBase customerBase = new CustomerBase();
		BeanUtils.copyProperties(customerBaseRequest, customerBase);
		// TODO 交易密码判空，非空另做加密修改
		mapper.insert(customerBase);
		respMap.put("code", BizResponseCode.OK.getCode());
		respMap.put("message", BizResponseCode.OK.getMessage());
		log.info("【添加客户基础信息】   响应参数：{}", respMap);
		return respMap;
	}

	/**
	 * 修改客户基础信息
	 * 
	 * @author centerroot
	 * @time 创建时间:2018年7月16日下午6:02:22
	 * @param customerBaseRequest
	 * @return
	 */
	public Map<String, Object> update(CustomerBaseRequest customerBaseRequest) {
		log.info("【修改客户基础信息】   请求参数：{}", EntityUtils.beanToMap(customerBaseRequest));
		Map<String, Object> respMap = new HashMap<String, Object>();
		CustomerBase CustomerBase = new CustomerBase();
		BeanUtils.copyProperties(customerBaseRequest, CustomerBase);
		// TODO 交易密码判空，非空另做加密修改
		mapper.updateByPrimaryKey(CustomerBase);
		respMap.put("code", BizResponseCode.OK.getCode());
		respMap.put("message", BizResponseCode.OK.getMessage());
		log.info("【修改客户基础信息】   响应参数：{}", respMap);
		return respMap;
	}

	/**
	 * 删除客户基础信息
	 * 
	 * @author centerroot
	 * @time 创建时间:2018年7月16日下午6:02:30
	 * @param
	 * @return
	 */
	public Map<String, Object> delete(
			@ApiParam("客户编号") @RequestParam(value = "customerId", required = true) String customerId) {
		log.info("【删除客户基础信息】   请求参数：{}", customerId);
		Map<String, Object> respMap = new HashMap<String, Object>();
		int count = mapper.deleteByPrimaryKey(customerId);
		respMap.put("code", BizResponseCode.OK.getCode());
		respMap.put("message", BizResponseCode.OK.getMessage());
		log.info("【删除客户基础信息】   响应参数：{}", respMap);
		return respMap;
	}

	public Map<String, Object> queryCustInfoAndAccountInfo(String merchantId) {
		Map<String, Object> result = new HashMap<>();
		MerchantBase merchantBase = new MerchantBase();
		merchantBase.setMerchantId(merchantId);
		merchantBase = merchantBaseMapper.selectOne(merchantBase);
		result.put("customerBase", merchantBase);
		MerchantAccount merchantAccount = new MerchantAccount();
		merchantAccount.setMerchantId(merchantId);
		List<MerchantAccount> accountList = merchantAccountMapper.select(merchantAccount);
		result.put("accountList", accountList);
		result.put("code", "0000");
		result.put("message", "查询成功");
		return result;
	}

	/**
	 * 用户个人信息注册
	 * @time 创建时间:2018年9月7日下午1:59:51
	 * @param registerRequest
	 * @return
	 */
	public CustomerBase register(RegisterRequest registerRequest) {
		log.info("【用户个人信息注册】   请求参数：{}", EntityUtils.beanToMap(registerRequest));
		String customerId = String.valueOf(SnowFlake.getId());
		String userId = String.valueOf(SnowFlake.getId());
		if(null != registerRequest.getIsOwner() && 1 == registerRequest.getIsOwner()){
			userId = registerRequest.getOwnerId();
		}
		String customerType = registerRequest.getCustomerType();
		String md5Password = DigestUtils.md5DigestAsHex((DigestUtils.md5DigestAsHex(registerRequest.getPassword().getBytes())+ UserConstant.PW_MD5_SALT).getBytes());
		String password = new BCryptPasswordEncoder(UserConstant.PW_ENCORDER_SALT).encode(md5Password);
		String passwordSalt = ObjectUtils.toString((new Random().nextInt() * (99999 - 10000 + 1)) + 10000);
		CustomerBase customerBase = new CustomerBase();
		customerBase.setPassword(password);
		customerBase.setPlatformId(registerRequest.getPlatformId());
		customerBase.setCustomerId(customerId);
		customerBase.setPasswordErrorCount(0);
		customerBase.setPayPasswordErrorCount(0);
		customerBase.setPasswordStatus("00");
		customerBase.setPayPasswordStatus("00");
		customerBase.setStatus("00");
		customerBase.setAuthenticationStatus(registerRequest.getAuthenticationStatus());
		if(org.apache.commons.lang3.StringUtils.isNotBlank(registerRequest.getAuthenticationStatus())){
			customerBase.setAuthenticationStatus(registerRequest.getAuthenticationStatus());
		}
		customerBase.setPasswordSalt(passwordSalt);
		customerBase.setCreateTime(new Timestamp(System.currentTimeMillis()));
		// 个人信息注册
		if (CustomerType.CUST_PERSONAL.getCode().equals(customerType)) {
			PersonalBase personalBase = new PersonalBase();
			PersonalBaseExt personalBaseExt = new PersonalBaseExt();
			BeanUtils.copyProperties(registerRequest,personalBase);
			if(null != registerRequest.getIsOwner() && 1 == registerRequest.getIsOwner()){
				personalBase.setOwnerId(registerRequest.getOwnerId());
				personalBase.setOwnerName(registerRequest.getRealName());
			}
			customerBase.setPersonalId(userId);
			customerBase.setCustomerType(CustomerType.CUST_PERSONAL.getCode());
			personalBase.setPersonalId(userId);
			personalBase.setCustomerId(customerId);
			personalBase.setAuthenticationStatus(registerRequest.getAuthenticationStatus());
			if(org.apache.commons.lang3.StringUtils.isNotBlank(registerRequest.getAuthenticationStatus())){
				personalBase.setAuthenticationStatus(registerRequest.getAuthenticationStatus());
			}
			personalBase.setStatus("00");
			personalBase.setPersonalType("00");
			personalBase.setCreateTime(new Date());
			personalBase.setRemark(registerRequest.getRemark());
			personalBaseExt.setEmail(registerRequest.getEmail());
			personalBaseExt.setPersonalLevel(registerRequest.getPersonalLevel());
			personalBaseExt.setPersonalId(userId);
			mapper.insertSelective(customerBase);
			personalBaseMapper.insertSelective(personalBase);
			personalBaseExtMapper.insertSelective(personalBaseExt);
		} else if (CustomerType.CUST_MERCHANT.getCode().equals(customerType)) {
			// 商户信息注册
			MerchantBase merchantBase = new MerchantBase();
			customerBase.setMerchantId(userId);
			customerBase.setUsername(registerRequest.getUsername());
			customerBase.setCustomerType(CustomerType.CUST_MERCHANT.getCode());
			merchantBase.setMerchantId(userId);
			merchantBase.setPlatformId(registerRequest.getPlatformId());
			merchantBase.setCustomerId(customerId);
			merchantBase.setMerchantLevel("00");
			merchantBase.setAuthenticationStatus("00");
			merchantBase.setStatus("00");
            merchantBase.setMerchantType(registerRequest.getMerchantType());
			merchantBase.setCreateTime(new Date());
			mapper.insertSelective(customerBase);
			merchantBaseMapper.insertSelective(merchantBase);
			PlatformAccoutConfig queryaccoutConfig = new PlatformAccoutConfig();
			queryaccoutConfig.setPlatformId(registerRequest.getPlatformId());
			queryaccoutConfig.setCustomerType(registerRequest.getCustomerType());
			//支持的账户类型
			List<PlatformAccoutConfig> accoutConfigs = platformAccoutConfigMapper.select(queryaccoutConfig);
			//平台支持的账户类型不存在 就没法开户
			if (null != accoutConfigs) {
				for (PlatformAccoutConfig accout: accoutConfigs){
					MerchantAccount merchantAccount = new MerchantAccount();
					String accountId = String.valueOf(SnowFlake.getId());
					merchantAccount.setAccountId(accountId);
					merchantAccount.setPlatformId(accout.getPlatformId());
					merchantAccount.setCustomerId(customerId);
					merchantAccount.setMerchantId(merchantBase.getMerchantId());
					merchantAccount.setMerchantType(merchantBase.getMerchantType());
					merchantAccount.setAccountType(accout.getAccountType());
					merchantAccount.setCcy("CNY");
					merchantAccount.setFreezeBalance(BigDecimal.ZERO);
					merchantAccount.setAccountBalance(BigDecimal.ZERO);
					merchantAccount.setCheckSum(MD5.sign(CurrencyUtils.decimaltoString(merchantAccount.getAccountBalance()), balanceSaltValue, "utf-8"));
					merchantAccount.setStatus(AccountState.normal.getCode());
					merchantAccount.setCreateTime(new Date());
					merAccountMapper.insertSelective(merchantAccount);
				}
			}
		}
		return customerBase;
	}

	/**
	 * 用户登录
	 * @time 创建时间:2018年9月8日下午4:47:04
	 * @param loginRequest
	 * @return
	 */
	public CustomerBase login(LoginRequest loginRequest) {
		log.info("【用户登录】   请求参数：{}", EntityUtils.beanToMap(loginRequest));
		CustomerBase customerBase = new CustomerBase();
		customerBase.setPlatformId(loginRequest.getPlatformId());
		customerBase.setCustomerId(loginRequest.getCustomerId());
		customerBase = mapper.selectOne(customerBase);
		if (null == customerBase) {
			throw new BusinessException(ResponseCode.USERINFO_NOTEXIST);
		}
		String md5Password = DigestUtils.md5DigestAsHex((DigestUtils.md5DigestAsHex(loginRequest.getPassword().getBytes())+ UserConstant.PW_MD5_SALT).getBytes());
		if (!encoder.matches(md5Password, customerBase.getPassword())) {
			throw new BusinessException(ResponseCode.USERINFO_PWD_ERROR);
		}
		return customerBase;
	}

	/**
	 * setupPayPassword 设置/重置交易密码
	 * 
	 * @author centerroot
	 * @time 创建时间:2018年9月25日上午10:54:44
	 * @param customerSetupPasswordRequest
	 * @return
	 * @throws Exception
	 */
	public Map<String, Object> setupPayPassword(
			@RequestBody @Valid CustomerSetupPasswordRequest customerSetupPasswordRequest) {
		log.info("【设置/重置交易密码】   请求参数：{}", EntityUtils.beanToMap(customerSetupPasswordRequest));
		Map<String, Object> respMap = new HashMap<String, Object>();
		CustomerBase customerBaseReq = new CustomerBase();
		customerBaseReq.setPlatformId(customerSetupPasswordRequest.getPlatformId());
		customerBaseReq.setCustomerId(customerSetupPasswordRequest.getCustomerId());
		CustomerBase customerBase = mapper.selectOne(customerBaseReq);
		if (null == customerBase) {
			respMap.put("code", BizResponseCode.CUST_NOTEXIST.getCode());
			respMap.put("message", BizResponseCode.CUST_NOTEXIST.getMessage());
			log.info("【设置/重置交易密码】   响应参数：{}", respMap);
			return respMap;
		}
		if (customerSetupPasswordRequest.getNewPassword().equals(customerSetupPasswordRequest.getConfirmPassword())) {
			//加密
			PlatformSafetyConfig platformSafetyConfigReq = new PlatformSafetyConfig();
			platformSafetyConfigReq.setPlatformId(customerSetupPasswordRequest.getPlatformId());
			PlatformSafetyConfig platformSafetyConfig = platformSafetyConfigMapper.selectOne(platformSafetyConfigReq);
			if (platformSafetyConfig != null) {
				String appKey = platformSafetyConfig.getAppKey();
				if (!StringUtils.isEmpty(appKey)) {
					String newPwd = SHA256Utils.SHA256(SHA256Utils.SHA256(customerSetupPasswordRequest.getNewPassword())+appKey);
					customerBase.setPayPassword(newPwd);
				}
			}
			customerBase.setPayPasswordErrorCount(0);
			customerBase.setPayPasswordStatus("00");
			mapper.updateByPrimaryKeySelective(customerBase);

			respMap.put("code", BizResponseCode.OK.getCode());
			respMap.put("message", BizResponseCode.OK.getMessage());
		} else {
			respMap.put("code", BizResponseCode.PAY_PWD_CONFIRM_ERROR.getCode());
			respMap.put("message", BizResponseCode.PAY_PWD_CONFIRM_ERROR.getMessage());
		}

		log.info("【设置/重置交易密码】   响应参数：{}", respMap);
		return respMap;
	}

	/**
	 * verifyPayPassword 校验交易密码
	 * 
	 * @author centerroot
	 * @time 创建时间:2018年9月25日上午10:54:37
	 * @param customerVerifyPasswordRequest
	 * @return
	 * @throws Exception
	 */
	public Map<String, Object> verifyPayPassword(
			@RequestBody @Valid CustomerVerifyPasswordRequest customerVerifyPasswordRequest) {
		log.info("【校验交易密码】   请求参数：{}", EntityUtils.beanToMap(customerVerifyPasswordRequest));
		Map<String, Object> respMap = new HashMap<String, Object>();
		CustomerBase customerBaseReq = new CustomerBase();
		customerBaseReq.setPlatformId(customerVerifyPasswordRequest.getPlatformId());
		if (null == customerVerifyPasswordRequest.getCustomerId()) {
			PersonalBase personalBaseReq = new PersonalBase();
			personalBaseReq.setPlatformId(customerVerifyPasswordRequest.getPlatformId());
			personalBaseReq.setRealName(customerVerifyPasswordRequest.getRealName());
			personalBaseReq.setIdCard(customerVerifyPasswordRequest.getCredNo());
			PersonalBase personalBase = personalBaseMapper.selectOne(personalBaseReq);
			if (null == personalBase) {
				respMap.put("code", BizResponseCode.PER_NOTEXIST.getCode());
				respMap.put("message", BizResponseCode.PER_NOTEXIST.getMessage());
				log.info("【校验交易密码】   响应参数：{}", respMap);
				return respMap;
			}
			customerBaseReq.setCustomerId(personalBase.getCustomerId());
		} else {
			customerBaseReq.setCustomerId(customerVerifyPasswordRequest.getCustomerId());
		}
		CustomerBase customerBase = mapper.selectOne(customerBaseReq);
		if (null == customerBase) {
			respMap.put("code", BizResponseCode.CUST_NOTEXIST.getCode());
			respMap.put("message", BizResponseCode.CUST_NOTEXIST.getMessage());
			log.info("【校验交易密码】   响应参数：{}", respMap);
			return respMap;
		}
		//加密
		PlatformSafetyConfig platformSafetyConfigReq = new PlatformSafetyConfig();
		platformSafetyConfigReq.setPlatformId(customerVerifyPasswordRequest.getPlatformId());
		PlatformSafetyConfig platformSafetyConfig = platformSafetyConfigMapper.selectOne(platformSafetyConfigReq);
		if (platformSafetyConfig != null) {
			String appKey = platformSafetyConfig.getAppKey();
			if (!StringUtils.isEmpty(appKey)) {
				if (SHA256Utils.SHA256(SHA256Utils.SHA256(customerVerifyPasswordRequest.getPassword())+appKey).equals(customerBase.getPayPassword())) {
					respMap.put("verifyFlag", true);
				} else {
					respMap.put("verifyFlag", false);
				}
			}
		}

		respMap.put("code", BizResponseCode.OK.getCode());
		respMap.put("message", BizResponseCode.OK.getMessage());
		log.info("【校验交易密码】   响应参数：{}", respMap);
		return respMap;
	}

	/**
	 * updatePayPassword 修改交易密码
	 * 
	 * @author centerroot
	 * @time 创建时间:2018年9月25日上午10:54:30
	 * @param customerUpdatePasswordRequest
	 * @return
	 * @throws Exception
	 */
	public Map<String, Object> updatePayPassword(
			@RequestBody @Valid CustomerUpdatePasswordRequest customerUpdatePasswordRequest) {
		log.info("【修改交易密码】   请求参数：{}", EntityUtils.beanToMap(customerUpdatePasswordRequest));
		Map<String, Object> respMap = new HashMap<String, Object>();
		CustomerBase customerBaseReq = new CustomerBase();
		customerBaseReq.setPlatformId(customerUpdatePasswordRequest.getPlatformId());
		customerBaseReq.setCustomerId(customerUpdatePasswordRequest.getCustomerId());
		CustomerBase customerBase = mapper.selectOne(customerBaseReq);
		if (null == customerBase) {
			respMap.put("code", BizResponseCode.CUST_NOTEXIST.getCode());
			respMap.put("message", BizResponseCode.CUST_NOTEXIST.getMessage());
			log.info("【设置/重置交易密码】   响应参数：{}", respMap);
			return respMap;
		}
		if (!customerUpdatePasswordRequest.getPassword().equals(customerBase.getPayPassword())) {
			respMap.put("code", BizResponseCode.PAY_PWD_ERROR.getCode());
			respMap.put("message", BizResponseCode.PAY_PWD_ERROR.getMessage());
			log.info("【设置/重置交易密码】   响应参数：{}", respMap);
			return respMap;
		}

		if (customerUpdatePasswordRequest.getNewPassword().equals(customerUpdatePasswordRequest.getConfirmPassword())) {
			customerBase.setPayPassword(customerUpdatePasswordRequest.getNewPassword());
			customerBase.setPayPasswordErrorCount(0);
			customerBase.setPayPasswordStatus("00");
			mapper.updateByPrimaryKeySelective(customerBase);

			respMap.put("code", BizResponseCode.OK.getCode());
			respMap.put("message", BizResponseCode.OK.getMessage());
		} else {
			respMap.put("code", BizResponseCode.PAY_PWD_CONFIRM_ERROR.getCode());
			respMap.put("message", BizResponseCode.PAY_PWD_CONFIRM_ERROR.getMessage());
		}

		log.info("【修改交易密码】   响应参数：{}", respMap);
		return respMap;
	}

	/**
	 * 会员实名信息列表查询
	 * @author centerroot
	 * @time 创建时间:2018年9月25日下午4:09:14
	 * @param customerRealNameRequest
	 * @return
	 */
	public Map<String, Object> queryCustRealNameList(CustomerRealNameRequest customerRealNameRequest) {
		log.info("【会员实名信息列表查询】   请求参数：{}", EntityUtils.beanToMap(customerRealNameRequest));
		Map<String, Object> respMap = new HashMap<String, Object>();
		int page = customerRealNameRequest.getPage();
		int limit = customerRealNameRequest.getLimit();
		Map<String, Object> params = new HashMap<String, Object>();
		params.put("page", page);
		params.put("limit", limit);
		Query query = new Query(params);
		Page<Object> result = PageHelper.startPage(query.getPage(), query.getLimit());
		PersonalCertificationInfo certificationInfoReq = new PersonalCertificationInfo();
		certificationInfoReq.setPlatformId(customerRealNameRequest.getPlatformId());
		certificationInfoReq.setPersonalId(customerRealNameRequest.getPersonalId());
		List<PersonalCertificationInfo> certificationInfoList = personalCertificationInfoMapper.select(certificationInfoReq);
		TableResultResponse<PersonalCertificationInfo> table = new TableResultResponse<PersonalCertificationInfo>(result.getTotal(), certificationInfoList);
		respMap.put("body", table);
		respMap.put("code", BizResponseCode.OK.getCode());
		respMap.put("message", BizResponseCode.OK.getMessage());
		log.info("【会员实名信息列表查询】   响应参数：{}", respMap);
		return respMap;
	}
	
	
	//############################小程序####################################################################################################
		/**
		 * 设置交易密码
		 * 
		 * @return
		 * @throws Exception
		 */
		public Map<String, Object> wechatSetupPayPassword(@RequestBody @Valid WechatSetupPasswordRequest wechatSetupPasswordRequest) {
			log.info("【设置交易密码】   请求参数：{}", EntityUtils.beanToMap(wechatSetupPasswordRequest));
			Map<String, Object> respMap = new HashMap<String, Object>();
			CustomerBase customerBaseReq = new CustomerBase();
			customerBaseReq.setPlatformId(wechatSetupPasswordRequest.getPlatformId());
			customerBaseReq.setCustomerId(wechatSetupPasswordRequest.getCustomerId());
			CustomerBase customerBase = mapper.selectOne(customerBaseReq);
			if (null == customerBase) {
				respMap.put("code", BizResponseCode.CUST_NOTEXIST.getCode());
				respMap.put("msg", BizResponseCode.CUST_NOTEXIST.getMessage());
				log.info("【设置交易密码】   用户不存在，响应参数：{}", respMap);
				return respMap;
			}
			
			PersonalBase personalBaseReq = new PersonalBase();
			personalBaseReq.setPersonalId(wechatSetupPasswordRequest.getPersonalId());
			personalBaseReq.setPlatformId(wechatSetupPasswordRequest.getPlatformId());
			personalBaseReq.setCustomerId(wechatSetupPasswordRequest.getCustomerId());
			personalBaseReq.setPersonalType("00");//注册用户
			PersonalBase personalBase = personalBaseMapper.selectOne(personalBaseReq);
			if (null == personalBase) {
				respMap.put("code", BizResponseCode.PER_NOTEXIST.getCode());
				respMap.put("msg", BizResponseCode.PER_NOTEXIST.getMessage());
				log.info("【设置交易密码】   个人用户不存在，响应参数：{}", respMap);
				return respMap;
			}
			
			ValidateSMSRequest validateSMSRequest = new ValidateSMSRequest();
			validateSMSRequest.setPhone(wechatSetupPasswordRequest.getPhone());
			validateSMSRequest.setCode(wechatSetupPasswordRequest.getSmsCode());
			 smsBiz.validateSmsCode(validateSMSRequest);

			if (wechatSetupPasswordRequest.getNewPassword().equals(wechatSetupPasswordRequest.getConfirmPassword())) {
				//加密
				PlatformSafetyConfig platformSafetyConfigReq = new PlatformSafetyConfig();
				platformSafetyConfigReq.setPlatformId(wechatSetupPasswordRequest.getPlatformId());
				PlatformSafetyConfig platformSafetyConfig = platformSafetyConfigMapper.selectOne(platformSafetyConfigReq);
				if (platformSafetyConfig != null) {
					String appKey = platformSafetyConfig.getAppKey();
					if (!StringUtils.isEmpty(appKey)) {
						String newPwd = SHA256Utils.SHA256(SHA256Utils.SHA256(wechatSetupPasswordRequest.getNewPassword())+appKey);
						customerBase.setPayPassword(newPwd);
					}else {
						respMap.put("code", BizResponseCode.PLATFORM_SAFETY_CONFIG_NO_EXIST.getCode());
						respMap.put("msg", BizResponseCode.PLATFORM_SAFETY_CONFIG_NO_EXIST.getMessage());
						return respMap;
					}
				}else {
					//UNIONID_IS_NULL
					respMap.put("code", BizResponseCode.PLATFORM_SAFETY_CONFIG_NO_EXIST.getCode());
					respMap.put("msg", BizResponseCode.PLATFORM_SAFETY_CONFIG_NO_EXIST.getMessage());
					return respMap;
				}
				customerBase.setPayPasswordErrorCount(0);
				customerBase.setPayPasswordStatus("00");
				mapper.updateByPrimaryKeySelective(customerBase);
				
				personalBase.setPhone(wechatSetupPasswordRequest.getPhone());
				personalBaseMapper.updateByPrimaryKeySelective(personalBase);

				respMap.put("phone", wechatSetupPasswordRequest.getPhone());
				respMap.put("code", BizResponseCode.OK.getCode());
				respMap.put("msg", BizResponseCode.OK.getMessage());
			} else {
				respMap.put("code", BizResponseCode.PAY_PWD_CONFIRM_ERROR.getCode());
				respMap.put("msg", BizResponseCode.PAY_PWD_CONFIRM_ERROR.getMessage());
				return respMap;
			}
			log.info("【设置交易密码】   响应参数：{}", respMap);
			return respMap;
		}
		
		
		
		
		/**
		 * 修改交易密码
		 * 
		 * @return
		 * @throws Exception
		 */
		public Map<String, Object> wechatUpdatePayPassword(@RequestBody @Valid WechatUpdatePasswordRequest wechatUpdatePasswordRequest) {
			log.info("【修改交易密码】   请求参数：{}", EntityUtils.beanToMap(wechatUpdatePasswordRequest));
			Map<String, Object> respMap = new HashMap<String, Object>();
			CustomerBase customerBaseReq = new CustomerBase();
			customerBaseReq.setPlatformId(wechatUpdatePasswordRequest.getPlatformId());
			customerBaseReq.setCustomerId(wechatUpdatePasswordRequest.getCustomerId());
			CustomerBase customerBase = mapper.selectOne(customerBaseReq);
			if (null == customerBase) {
				respMap.put("code", BizResponseCode.CUST_NOTEXIST.getCode());
				respMap.put("msg", BizResponseCode.CUST_NOTEXIST.getMessage());
				log.info("【设置/重置交易密码】   响应参数：{}", respMap);
				return respMap;
			}
			
			ValidateSMSRequest validateSMSRequest = new ValidateSMSRequest();
			validateSMSRequest.setPhone(wechatUpdatePasswordRequest.getPhone());
			validateSMSRequest.setCode(wechatUpdatePasswordRequest.getSmsCode());
			smsBiz.validateSmsCode(validateSMSRequest);

			if (wechatUpdatePasswordRequest.getNewPassword().equals(wechatUpdatePasswordRequest.getConfirmPassword())) {
				//加密
				PlatformSafetyConfig platformSafetyConfigReq = new PlatformSafetyConfig();
				platformSafetyConfigReq.setPlatformId(wechatUpdatePasswordRequest.getPlatformId());
				PlatformSafetyConfig platformSafetyConfig = platformSafetyConfigMapper.selectOne(platformSafetyConfigReq);
				if (platformSafetyConfig != null) {
					String appKey = platformSafetyConfig.getAppKey();
					if (!StringUtils.isEmpty(appKey)) {
						String newPwd = SHA256Utils.SHA256(SHA256Utils.SHA256(wechatUpdatePasswordRequest.getNewPassword())+appKey);
						customerBase.setPayPassword(newPwd);
					}else {
						respMap.put("code", BizResponseCode.PLATFORM_SAFETY_CONFIG_NO_EXIST.getCode());
						respMap.put("msg", BizResponseCode.PLATFORM_SAFETY_CONFIG_NO_EXIST.getMessage());
						return respMap;
					}
				}else {
					//UNIONID_IS_NULL
					respMap.put("code", BizResponseCode.UNIONID_IS_NULL.getCode());
					respMap.put("msg", BizResponseCode.UNIONID_IS_NULL.getMessage());
					return respMap;
				}
				customerBase.setPayPasswordErrorCount(0);
				customerBase.setPayPasswordStatus("00");
				mapper.updateByPrimaryKeySelective(customerBase);
				
				respMap.put("code", BizResponseCode.OK.getCode());
				respMap.put("msg", BizResponseCode.OK.getMessage());
			} else {
				respMap.put("code", BizResponseCode.PAY_PWD_CONFIRM_ERROR.getCode());
				respMap.put("msg", BizResponseCode.PAY_PWD_CONFIRM_ERROR.getMessage());
			}

			log.info("【修改交易密码】   响应参数：{}", respMap);
			return respMap;
		}
}