package me.flyray.crm.core.biz.platform;

import java.math.BigDecimal;
import java.util.Date;
import java.util.Map;

import me.flyray.crm.core.blockchain.ERC827Contract;
import me.flyray.crm.core.blockchain.util.KeyUtil;
import me.flyray.crm.core.blockchain.wallet.Wallet;
import me.flyray.crm.core.entity.PersonalBase;
import me.flyray.crm.core.entity.PlatformCoinCustomer;
import me.flyray.crm.core.mapper.PersonalBaseMapper;
import me.flyray.crm.core.mapper.PlatformCoinCustomerMapper;
import org.apache.commons.lang3.StringUtils;
import org.bouncycastle.jcajce.provider.asymmetric.ec.BCECPrivateKey;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import me.flyray.common.biz.BaseBiz;
import me.flyray.common.util.MD5;

import lombok.extern.slf4j.Slf4j;

/**
 * 平台唯一用户表
 * @author he
 * @date 2018-09-08 14:08:33
 */
@Slf4j
@Service
public class PlatformCoinCustomerBiz extends BaseBiz<PlatformCoinCustomerMapper, PlatformCoinCustomer> {

	@Autowired
	private PersonalBaseMapper personalBaseMapper;
	@Autowired
	private PlatformCoinCustomerMapper platformCoinCustomerMapper;
	@Value("${crm.account.balanceSaltValue}")
	private String balanceSaltValue;
	@Value("${toplevel.platform.id}")
	private String toplevelPlatformId;
	/**
	 * 微信小程序火源账户信息查询
	 * @param request
	 * @return
	 */
	public PlatformCoinCustomer wechatMiniFireSourceInfo(Map<String, Object> request){
		log.info("微信小程序火源账户信息查询.......{}", request);
		PersonalBase personalBase = new PersonalBase();
		personalBase.setPlatformId((String)request.get("platformId"));
		personalBase.setCustomerId((String)request.get("customerId"));
		personalBase.setPersonalId((String)request.get("personalId"));
		PersonalBase selectPersonalBase = personalBaseMapper.selectOne(personalBase);
		if(null != selectPersonalBase && !StringUtils.isEmpty(selectPersonalBase.getIdCard())){
			PlatformCoinCustomer platformCoinCustomer = new PlatformCoinCustomer();
			platformCoinCustomer.setPlatformId(toplevelPlatformId);//顶级平台编号
			platformCoinCustomer.setIdCard(selectPersonalBase.getIdCard());
			PlatformCoinCustomer selectCoinCustomer = platformCoinCustomerMapper.selectOne(platformCoinCustomer);
			log.info("微信小程序火源账户信息查询结束.......");
			return selectCoinCustomer;
		}else{
			PlatformCoinCustomer platformUniqueCustomer = new PlatformCoinCustomer();
			log.info("微信小程序火源账户信息查询结束.......");
			return platformUniqueCustomer;
		}
	}

	/**
	 * 火源账户钱包创建
	 * @param request
	 * @return
	 */
	public PersonalBase createFireSourceWallet(Map<String, Object> request){
		log.info("火源账户钱包创建.......{}", request);
		PersonalBase personalBase = new PersonalBase();
		personalBase.setPlatformId((String)request.get("platformId"));
		personalBase.setCustomerId((String)request.get("customerId"));
		personalBase.setPersonalId((String)request.get("personalId"));
		PersonalBase selectPersonalBase = personalBaseMapper.selectOne(personalBase);
		if(null != selectPersonalBase){
			selectPersonalBase.setRealName((String)request.get("realName"));
			selectPersonalBase.setIdCard((String)request.get("idCard"));
			personalBaseMapper.updateByPrimaryKey(selectPersonalBase);

			PlatformCoinCustomer platformCoinCustomer = new PlatformCoinCustomer();
			platformCoinCustomer.setPlatformId(toplevelPlatformId);//顶级平台编号
			platformCoinCustomer.setIdCard(selectPersonalBase.getIdCard());
			PlatformCoinCustomer selectCoinCustomer = platformCoinCustomerMapper.selectOne(platformCoinCustomer);
			if(null == selectCoinCustomer){
				platformCoinCustomer.setCreateTime(new Date());
				platformCoinCustomer.setBalance(BigDecimal.ZERO);
				platformCoinCustomer.setCheckSum(MD5.sign("0", balanceSaltValue, "utf-8"));
				platformCoinCustomer.setFreezeBalance(BigDecimal.ZERO);
				platformCoinCustomer.setRealName((String)request.get("realName"));
				try {
					Wallet wallet = ERC827Contract.createWallet();
		        	String address = wallet.getAddress();
		        	BCECPrivateKey privateKey = wallet.getPrivateKey();
		        	//用户私钥保存私钥用户验证交易
		        	String privateKeyStr = KeyUtil.privateKeyToString(privateKey);
					platformCoinCustomer.setAddress(address);
					platformCoinCustomer.setPrivateKey(privateKeyStr);
					
				} catch (Exception e) {
					e.printStackTrace();
				}
				platformCoinCustomerMapper.insert(platformCoinCustomer);
			}
		}
		return selectPersonalBase;
	}
	
}