package me.flyray.crm.core.api;

import java.util.Map;

import javax.validation.Valid;

import me.flyray.crm.facade.request.WechatMiniProgramAuthParam;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import me.flyray.common.util.EntityUtils;
import me.flyray.crm.core.biz.customer.CustomerBaseAuthBiz;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

@Api(tags="授权管理")
@Controller
@RequestMapping("auths")
public class AuthsController {
	
	@Autowired
	private CustomerBaseAuthBiz customerBaseAuthsBiz;
	
	/**
	 * 微信小程序授权
	 * @return
	 * @throws Exception 
	 */ 
	@ApiOperation("微信小程序授权")
	@RequestMapping(value = "/wechatMiniProgram",method = RequestMethod.POST)
    @ResponseBody
    public Map<String, Object> wechatMiniProgramAuth(@RequestBody @Valid WechatMiniProgramAuthParam wechatMiniProgramAuthParam) throws Exception {
		Map<String, Object> response = customerBaseAuthsBiz.wechatMiniProgramAuth(EntityUtils.beanToMap(wechatMiniProgramAuthParam));
		return response;
    }

}
