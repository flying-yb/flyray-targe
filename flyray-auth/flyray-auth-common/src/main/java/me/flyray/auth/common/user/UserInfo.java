package me.flyray.auth.common.user;

import lombok.Data;

import java.io.Serializable;

/**
 * ${DESCRIPTION}
 * @create 2017-06-21 8:12
 */

@Data
public class UserInfo implements Serializable{

    public String id;
    public String userId;
    private String nickname;
    public String username;
    public String password;
    public String appId;
    private String description;
    private Integer userType;
    private String platformId;
    private String merchantId;
    private String skinStyle;
    private String mobilePhone;

}
