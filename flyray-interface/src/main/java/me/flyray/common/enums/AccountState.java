package me.flyray.common.enums;

/** 
* 账户状态标识
*/

public enum AccountState {

	normal("00","正常"),
	freeze("01","冻结");
	
    private String code;
    private String desc;
    
    private AccountState(String code, String desc){
        this.code = code;
        this.desc = desc;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getDesc() {
        return desc;
    }

    public void setDesc(String desc) {
        this.desc = desc;
    }
    
    public static AccountState getCommentModuleNo(String code){
        for(AccountState o : AccountState.values()){
            if(o.getCode().equals(code)){
                return o;
            }
        }
        return null;
    }
}
