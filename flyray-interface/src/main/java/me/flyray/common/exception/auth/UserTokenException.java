package me.flyray.common.exception.auth;


import me.flyray.common.constant.CommonConstants;
import me.flyray.common.exception.BaseException;

/**
 * Created by ace on 2017/9/8.
 */
public class UserTokenException extends BaseException {
    public UserTokenException(String message) {
        super(message, CommonConstants.EX_USER_INVALID_CODE);
    }
}
