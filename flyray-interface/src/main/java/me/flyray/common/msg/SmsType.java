package me.flyray.common.msg;

/**
 * 短信通知类型
 */
public enum SmsType {

	register("01","注册"),
    loginMobile("02","手机号+验证码登录"),
    addMobile("03","添加手机号"),
    changeMobile("04","更换手机号"),
    updatePaymentPassword("05","修改支付密码 "),
	resetPassword("06","重置登录密码 ");

    private String code;
    private String desc;

    private SmsType(String code, String desc) {
        this.code = code;
        this.desc = desc;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getDesc() {
        return desc;
    }

    public void setDesc(String desc) {
        this.desc = desc;
    }

    public static SmsType getSmsType(String code) {
        for (SmsType o : SmsType.values()) {
            if (o.getCode().equals(code)) {
                return o;
            }
        }
        return null;
    }
}
