package me.flyray.common.msg;

/**
 * 常量定义
 * @author centerroot
 * @time 创建时间:2018年9月8日下午3:13:51
 * @description
 */

public class Constant {

    //redis保存发送短信验证码Key
    public static final String SEND_CODE= "SEND_CODE_:";
    
    
    
    
    public static final String COUPON_CODE = "10";//红包
    public static final String INTEGRAL_CODE = "20";//积分
    public static final String RANDOM_CODE = "30";//随机立减
    /**
     * 红包类型
     * 0 普通红包
     * 1 随机红包
     * */
    public static final String COMMON_COUPON = "0";
    public static final String RANDOM_COUPON = "1";
}


