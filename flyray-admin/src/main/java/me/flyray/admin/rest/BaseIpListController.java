package me.flyray.admin.rest;

import me.flyray.admin.biz.BaseIpListBiz;
import me.flyray.admin.entity.BaseIpList;
import me.flyray.common.rest.BaseController;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("ipList")
public class BaseIpListController extends BaseController<BaseIpListBiz, BaseIpList> {
	
	
}