package me.flyray.admin.entity;

import lombok.Data;

import javax.persistence.*;

@Data
@Table(name = "base_role_application")
public class RoleApplication {
    @Id
    private Long id;

    /**
     * 角色ID
     */
    @Column(name = "role_id")
    private String roleId;

    /**
     * 菜单ID
     */
    @Column(name = "app_id")
    private String appId;

}