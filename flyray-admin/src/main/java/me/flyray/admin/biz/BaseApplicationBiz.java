package me.flyray.admin.biz;

import me.flyray.admin.mapper.DeptMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import me.flyray.admin.entity.BaseApplication;
import me.flyray.admin.mapper.BaseApplicationMapper;
import me.flyray.common.biz.BaseBiz;

import java.util.List;

/**
 * 
 *
 * @author admin
 * @email ${email}
 * @date 2019-03-05 19:50:36
 */
@Service
public class BaseApplicationBiz extends BaseBiz<BaseApplicationMapper,BaseApplication> {

    @Autowired
    private BaseApplicationMapper baseApplicationMapper;

    public List<BaseApplication> selectByRoleId(String roleId){
        return baseApplicationMapper.selectByRoleId(roleId);
    }

}