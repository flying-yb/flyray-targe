package me.flyray.admin.mapper;
import java.util.List;
import java.util.Map;

import me.flyray.admin.entity.BaseIpList;
import tk.mybatis.mapper.common.Mapper;

/**
 * 白名单
 * 
 * @author mu
 * @email ${email}
 * @date 2018-08-28 14:30:04
 */
public interface BaseIpListMapper extends Mapper<BaseIpList> {
	//获取所有的白名单信息
	List<BaseIpList> getAllBaseIpList();
	
}
